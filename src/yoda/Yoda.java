/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package yoda;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 *
 * @author simone
 */
public class Yoda {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException  {
       String path = "docs//new-treebank-POS-GOOGLE.txt";
       
       int tempinizio1 = 0; //contatori caratteri livello 1
       int tempfine1 = 0;
       
       int tempinizio2 = 0;  //contatori caratteri livello 2
       int tempfine2 = 0;
       
       ArrayList<ArrayList<String>> frasiLivUno = new ArrayList();  //costituenti di livello 1 (e sottoalbero) per tutte le frasi del file
       
       
       try {
            BufferedReader br = new BufferedReader(new FileReader(path));
            String line;
            
            while ((line = br.readLine()) != null) {       //per ogni riga
                
                ArrayList<String> livelliUno = new ArrayList(); // costituenti al 1 livello dell'albero, con relativo sottoalbero
               
                livelliUno = analizeLevel(line);                
                frasiLivUno.add(new ArrayList(livelliUno));
                livelliUno.clear();
            }
            
   
            //prendi una frase, vedi i livelli1
            
            int indice = 0;   //indice della frase da analizzare
            
            ArrayList<String> frase1 = new ArrayList();
            frase1 = frasiLivUno.get(indice);
            
            System.out.println(frase1);
            
            String soggetto = findSubject(frase1);      //ho ottenuto il soggetto della mia frase (da tradurre)
            
            System.out.println("SOGGETTO:\n     " + soggetto);
                        
            ArrayList<String> VP = analizeVP(frase1);   //elementi del VP; in prima posizione sempre il verbo, poi i complementi
            
            String verbo = VP.get(0);       //verbo, prima posizione del vettore del sottoalbero VP
            System.out.println("VERBO:\n    " + verbo);
            
            System.out.println("COMPLEMENTI:");     //complementi, nell'ordine in cui compaiono nel VP
            for(int i = 1; i<VP.size(); i++){
                System.out.println("    "+VP.get(i));
            }
            
            String trad = transferTranslator(soggetto, VP);
            
            System.out.println(trad);
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Yoda.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    private static boolean findNP(String albero){
        
        Pattern p = Pattern.compile("^(\\(NP)[\\sA-Za-z0-9\\w\\(\\)\\,\\.\\;\\:\\*\\'\\\"\\-\\_\\à\\è\\ì\\ò\\ù\\À\\+"
            + "È\\Ì\\Ò\\Ù\\á\\é\\í\\ó\\ú\\ý\\Á\\É\\Í\\Ó\\Ú\\Ý\\â\\ê\\î\\ô\\û\\Â\\Ê\\Î\\Ô\\Û\\ã\\ñ\\"
            + "õ\\Ã\\Ñ\\Õ\\ä\\ë\\ï\\ö\\ü\\ÿ\\Ä\\Ë\\Ï\\Ö\\Ü\\Ÿ\\ç\\Ç\\ß\\Ø\\ø\\Å\\å\\Æ\\æ\\œ]+");
        
       
        //per ogni elemento sul livello prendo il primo elemento che inizia per NP
        Matcher m = p.matcher(albero);

        if(m.find()) {   //se trova l'elemento
            String np = m.group();
            //System.out.println("IF   " + np);
            return true;
        }
        else 
            return false;
    }
    
    private static boolean findVP(String albero){
        
        Pattern p = Pattern.compile("^(\\(VP)[\\sA-Za-z0-9\\w\\(\\)\\,\\.\\;\\:\\*\\'\\\"\\-\\_\\à\\è\\ì\\ò\\ù\\À\\+"
            + "È\\Ì\\Ò\\Ù\\á\\é\\í\\ó\\ú\\ý\\Á\\É\\Í\\Ó\\Ú\\Ý\\â\\ê\\î\\ô\\û\\Â\\Ê\\Î\\Ô\\Û\\ã\\ñ\\"
            + "õ\\Ã\\Ñ\\Õ\\ä\\ë\\ï\\ö\\ü\\ÿ\\Ä\\Ë\\Ï\\Ö\\Ü\\Ÿ\\ç\\Ç\\ß\\Ø\\ø\\Å\\å\\Æ\\æ\\œ]+");
        
       
        //per ogni elemento sul livello prendo il primo elemento che inizia per NP
        Matcher m = p.matcher(albero);

        if(m.find()) {   //se trova l'elemento
            String vp = m.group();
            //System.out.println("IF   " + np);
            return true;
        }
        else 
            return false;
    }
    
    private static String findSubject(ArrayList<String> livello1){
        
        String subj = null;
            
            for(String elemento : livello1){
                
                if(findNP(elemento)) {  //se l'elemento di livello1 è di tipo NP, indica il soggetto della frase
                    subj = elemento;
                    break;      //non appena trovo il soggetto, esco dal ciclo
                }
            }
            System.out.println(subj);
        return subj;
    }
    
    
    private static ArrayList<String> analizeVP(ArrayList<String> livello1){
        String vp = null;
        String verb = null;
        ArrayList<String> verbComp = new ArrayList();
        
        
        Pattern p = Pattern.compile("^(\\(VERB)[\\sA-Za-z0-9\\w\\(\\)\\,\\.\\;\\:\\*\\'\\\"\\-\\_\\à\\è\\ì\\ò\\ù\\À\\+"
            + "È\\Ì\\Ò\\Ù\\á\\é\\í\\ó\\ú\\ý\\Á\\É\\Í\\Ó\\Ú\\Ý\\â\\ê\\î\\ô\\û\\Â\\Ê\\Î\\Ô\\Û\\ã\\ñ\\"
            + "õ\\Ã\\Ñ\\Õ\\ä\\ë\\ï\\ö\\ü\\ÿ\\Ä\\Ë\\Ï\\Ö\\Ü\\Ÿ\\ç\\Ç\\ß\\Ø\\ø\\Å\\å\\Æ\\æ\\œ]+");
        
            
        for(String elemento : livello1){

            if(findVP(elemento)) {  //se l'elemento di livello1 è di tipo NP, indica il soggetto della frase
                vp = elemento;
                break;      //non appena trovo il soggetto, esco dal ciclo
            }
        }
        
        //devo analizzare il vp e trovare il verbo all'interno
        ArrayList<String> elementiVP = analizeLevel(vp);
        
        for(String elemento : elementiVP){      //cerco in tutti i sottoalberi di VP
            
            Matcher m = p.matcher(elemento);        //quello che contiene il verbo
            
            if(m.find()){
                verbComp.add(elemento);
                break;
            }
        }
        
        
        
        //cerco in tutti i sottoalberi di VP i complementi (che siano (S, (NP o (PP
        Pattern pObj1 = Pattern.compile("^(\\(S)[\\sA-Za-z0-9\\w\\(\\)\\,\\.\\;\\:\\*\\'\\\"\\-\\_\\à\\è\\ì\\ò\\ù\\À\\+"
            + "È\\Ì\\Ò\\Ù\\á\\é\\í\\ó\\ú\\ý\\Á\\É\\Í\\Ó\\Ú\\Ý\\â\\ê\\î\\ô\\û\\Â\\Ê\\Î\\Ô\\Û\\ã\\ñ\\"
            + "õ\\Ã\\Ñ\\Õ\\ä\\ë\\ï\\ö\\ü\\ÿ\\Ä\\Ë\\Ï\\Ö\\Ü\\Ÿ\\ç\\Ç\\ß\\Ø\\ø\\Å\\å\\Æ\\æ\\œ]+");
        
        Pattern pObj2 = Pattern.compile("^(\\(NP)[\\sA-Za-z0-9\\w\\(\\)\\,\\.\\;\\:\\*\\'\\\"\\-\\_\\à\\è\\ì\\ò\\ù\\À\\+"
            + "È\\Ì\\Ò\\Ù\\á\\é\\í\\ó\\ú\\ý\\Á\\É\\Í\\Ó\\Ú\\Ý\\â\\ê\\î\\ô\\û\\Â\\Ê\\Î\\Ô\\Û\\ã\\ñ\\"
            + "õ\\Ã\\Ñ\\Õ\\ä\\ë\\ï\\ö\\ü\\ÿ\\Ä\\Ë\\Ï\\Ö\\Ü\\Ÿ\\ç\\Ç\\ß\\Ø\\ø\\Å\\å\\Æ\\æ\\œ]+");
        
        Pattern pObj3 = Pattern.compile("^(\\(PP)[\\sA-Za-z0-9\\w\\(\\)\\,\\.\\;\\:\\*\\'\\\"\\-\\_\\à\\è\\ì\\ò\\ù\\À\\+"
            + "È\\Ì\\Ò\\Ù\\á\\é\\í\\ó\\ú\\ý\\Á\\É\\Í\\Ó\\Ú\\Ý\\â\\ê\\î\\ô\\û\\Â\\Ê\\Î\\Ô\\Û\\ã\\ñ\\"
            + "õ\\Ã\\Ñ\\Õ\\ä\\ë\\ï\\ö\\ü\\ÿ\\Ä\\Ë\\Ï\\Ö\\Ü\\Ÿ\\ç\\Ç\\ß\\Ø\\ø\\Å\\å\\Æ\\æ\\œ]+");
        
        Pattern pObj4 = Pattern.compile("^(\\(PRN)[\\sA-Za-z0-9\\w\\(\\)\\,\\.\\;\\:\\*\\'\\\"\\-\\_\\à\\è\\ì\\ò\\ù\\À\\+"
            + "È\\Ì\\Ò\\Ù\\á\\é\\í\\ó\\ú\\ý\\Á\\É\\Í\\Ó\\Ú\\Ý\\â\\ê\\î\\ô\\û\\Â\\Ê\\Î\\Ô\\Û\\ã\\ñ\\"
            + "õ\\Ã\\Ñ\\Õ\\ä\\ë\\ï\\ö\\ü\\ÿ\\Ä\\Ë\\Ï\\Ö\\Ü\\Ÿ\\ç\\Ç\\ß\\Ø\\ø\\Å\\å\\Æ\\æ\\œ]+");
        
       
        
        elementiVP.stream().forEach((elemento) -> {     //ciclo strano, fatto direttamente da Java!
            Matcher mObj1 = pObj1.matcher(elemento);
            Matcher mObj2 = pObj2.matcher(elemento);
            Matcher mObj3 = pObj3.matcher(elemento);
            Matcher mObj4 = pObj4.matcher(elemento);
            
            while(mObj1.find() || mObj2.find() || mObj3.find() || mObj4.find()){
                
                verbComp.add(elemento);                
            }
        });
        
        
       // System.out.println("VP");
       // System.out.println(elementiVP);
        
        System.out.println("Elementi    :");
        System.out.println(verbComp);
        return verbComp;
    }
    
    
    private static ArrayList<String> analizeLevel(String line){
        
        ArrayList<String> levelElements = new ArrayList();
        
        char[] array = line.toCharArray();  //scorri carattere per carattere
        int livello = -1;   //contatore del livello corrente dell'albero
        int cont = 0;   //contatore indice del carattere corrente
        
        int tempinizio1 = 0;    //contatori di sottostringa del livello
        int tempfine1 = 0;
                
        for (char ch : array) {
            if (ch == '(')  //aperta parentesi, scendo di un livello nell'albero
            {   
                livello++;
                if (livello==1)
                    tempinizio1 = cont; //inizia il pezzo di livello 1
            }
            else if (ch == ')'){    //chiusa parentesi

                if (livello == 1){  //finisce il nodo di livello uno (con relativo sottoalbero)

                    tempfine1 = cont;
                    String liv = line.substring(tempinizio1, tempfine1+1);
                    levelElements.add(liv);    //inserisco il sottoalbero di liv 1 nella lista dei livelli 1 per la frase
                }
                livello--;  //scendo di livello
            }
            cont++;
        }
        return levelElements;        
                //frasiLivUno.add(new ArrayList(livelliUno));
                //livelliUno.clear();
    }
    
    private static String transferTranslator(String soggetto, ArrayList<String> verbComp){
        
        String fraseTradotta;
        
        //frase deve essere di tipo OSV!!
        fraseTradotta = "(S " ;     //prima radice
        
        if(verbComp.size() > 2)     //se ho più di due elementi in lista (più di un complemento), questi devono essere racchiusi in un unico costituente!
            fraseTradotta += "(NP ";  //?
        
        for(int i = 1; i < verbComp.size(); i++){       //poi complementi
            fraseTradotta += verbComp.get(i);
        }
        
        if(verbComp.size() > 2)  
            fraseTradotta += " )";
            
        //separatore ',' tra parte dei complementi e parte del soggetto
        fraseTradotta += "(. ,)";
        
        //poi soggetto
        
        fraseTradotta += "(VP " + soggetto;
        
        //poi verbo
        fraseTradotta += verbComp.get(0) + ")";
        
        
        fraseTradotta += ")";
        return fraseTradotta;
    }
    
}